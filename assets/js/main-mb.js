$(document).ready(function() {
    $(window).scroll(function () {
      if($(window).scrollTop() > 0) {
        $(".fix-social-navigation").css({"bottom": "0"});
      } else {
        $(".fix-social-navigation").css({"bottom": "-30px"});
      } 
    });
    // button menu
    $('.navbar-toggle').click(function() {
            $('body').addClass('fixed-scroll');
            $('.btn-menu').addClass('floatright');
            $('.close-menu').addClass('dark-bg');
    });
    // cancel icon
    $('.fixheight-icon').click(function() {
            $('.btn-menu').removeClass('floatright');
            $('body').removeClass('fixed-scroll');
            $('.close-menu').removeClass('dark-bg');
    });
    //dark background
     $('.close-menu').click(function() {
            $('.close-menu').removeClass('dark-bg');
            $('.btn-menu').removeClass('floatright');
            $('body').removeClass('fixed-scroll');
    });
    $('.dropdown').click(function() {
            $(this).children('ul').slideToggle(300);

    });
     $("#search").click(function(e){
        e.preventDefault();
        $("#search-form").addClass('form-group-change');
        // $(".glyphicon-search").css('display','none');
        // $(".glyphicon-remove").css('display','block');
        $("#search").addClass('glyphicon-search-hide');
        $("#search").removeClass('glyphicon-search-show');
        $("#close").addClass('glyphicon-remove-show');
        $("#close").removeClass('glyphicon-remove-hide');
    });
    $("#close").click(function(e){
     e.preventDefault();
      $("#search-form").removeClass('form-group-change');
      // $(".glyphicon-search").css('display','block');
      // $(".glyphicon-remove").css('display','none');
      $("#search").addClass('glyphicon-search-show');
      $("#search").removeClass('glyphicon-search-hide');
      $("#close").addClass('glyphicon-remove-hide');
      $("#close").removeClass('glyphicon-remove-show');
    });
    // comment box
    $(".cmtsecont").click(function(){
        $(".add-cmt").slideToggle(500);
    });
    // menu dropdown click
    $('#btn-sub-rt').click(function() {
            $('ul#sub-rt').slideToggle(300);
    });
    $('#btn-sub-of-sub-01').click(function() {
            $('ul#sub-menu-01').slideToggle(300);
    });
    $('#btn-sub-of-sub-02').click(function() {
            $('ul#sub-menu-02').slideToggle(300);
    });
    $('#btn-sub-of-sub-03').click(function() {
            $('ul#sub-menu-03').slideToggle(300);
    });
     $('#btn-sub-of-sub-04').click(function() {
            $('ul#sub-menu-04').slideToggle(300);
    });
     $('#btn-sub-of-sub-05').click(function() {
            $('ul#sub-menu-05').slideToggle(300);
    });
     $('#btn-sub-of-sub-06').click(function() {
            $('ul#sub-menu-06').slideToggle(300);
    });
     $('#btn-sub-of-sub-07').click(function() {
            $('ul#sub-menu-07').slideToggle(300);
    });
     $('#btn-sub-of-sub-08').click(function() {
            $('ul#sub-menu-08').slideToggle(300);
    });
     $('#btn-sub-of-sub-09').click(function() {
            $('ul#sub-menu-09').slideToggle(300);
    });
    $('.drop-menu-children').click(function() {
       var getId = $(this).attr('id');
       var array = getId.split('-');
       var id = array[array.length - 1];
       var idMenu = 'ul#sub-menu-' + id;
       $(idMenu).slideToggle(300);
   });
});
