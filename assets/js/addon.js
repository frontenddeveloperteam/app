$(document).ready(function() {
   var summaries = $('.fixed');
   summaries.each(function(i) {
     var summary = $(summaries[i]);
     var next = summaries[i + 1];
     summary.scrollToFixed({
       limit: function() {
         var limit = 0;
         if (next) {
             limit = $(next).offset().top - $(this).outerHeight(true) - 10;
         } else {
             limit = $('.change-scroll').offset().top - $(this).outerHeight(true) - 10;
         }
         return limit;
       },
       zIndex: 999
     });
   });
    var summaries = $('.change-scroll');
     summaries.each(function(i) {
       var summary = $(summaries[i]);
       var next = summaries[i + 1];
       summary.scrollToFixed({
         marginTop: - 15,
         limit: function() {
           var limit = 0;
           if (next) {
               limit = $(next).offset().top - $(this).outerHeight(true) - 10;
           } else {
               limit = $('.main-footer').offset().top - $(this).outerHeight(true) - 10;
           }
           return limit;
         },
         zIndex: 999
       });
     });
});

