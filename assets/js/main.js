$(document).ready(function() {
  // show button back to top
    $(window).scroll(function () {
      if($(".detail-news-center").height() < 2500) {
        $("#break-middle").hide();
      };
      if($(".detail-news-center").height() < 1800) {
        $("#break-middle").hide();
      };
      if($(".detail-news-center").height() < 1300) {
        $("#break-middle").hide();
        $("#break-bottom").hide();
      };
      /*sort content*/
      if($(".detail-news-center").height() < 800) {
        $("#break-top").hide();
        $("#break-middle").hide();
        $("#break-bottom").hide();
      };
      if($('.change-scroll').hasClass('scroll-to-fixed-fixed')){
        $('.change-scroll').addClass('transform');
        $('.site-header').hide();
        $('.hidden-home').show();
      } else {
        $('.change-scroll').removeClass('transform');
        $('.site-header').show();
        $('.hidden-home').hide();
      }
    });
    var texttotal = $(".string-text").text().length;
    if(texttotal < 60) {
      $(".string-text").css({"font-size":"22px"});
    };
    $('.back-top').hide();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 200) {
            $('.back-top').fadeIn();
        } else {
            $('.back-top').fadeOut();
        }
    });
    $('.back-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
    $('li.check').click(function() {
      $(this).toggleClass('check-active');
    });  
  //button menu
   $('#btn-show-menu').click(function() {
        $('.nav-upright').slideToggle(200);
    });
     // comment section
      $(".cmtsecont").click(function(){
      $(".comment-boxsecond").slideToggle(500);
    });
    // event scroll
    $(window).scroll(function() {
      if($(window).scrollTop() > 0) {
        $('.site-header').addClass('light-nav');
        $('.navbar-nav').addClass('light-element');
        $('.search-box button').addClass('dark-button');
        $('.arrow_box').addClass('dark-border');
        $('.search-button').addClass('event-scrool');
        $('.glyphicon-search').addClass('search-out');
        $('.icon-bar').addClass('icon-bar-scroll');
        $('.search-box span.glyphicon').addClass('glyphicon-scrool');
      } else {
        $('.site-header').removeClass('light-nav');
        $('.navbar-nav').removeClass('light-element');
        $('.search-box button').removeClass('dark-button');
        $('.arrow_box').removeClass('dark-border');
        $('.search-button').removeClass('event-scrool');
        $('.glyphicon-search').removeClass('search-out');
        $('.icon-bar').removeClass('icon-bar-scroll');
        $('.search-box span.glyphicon').removeClass('glyphicon-scrool');
      }
    });
    // search button
     $("#search").click(function(){
        $("#search-form").addClass('form-group-change');
        $("#search").addClass('glyphicon-search-hide');
        $("#search").removeClass('glyphicon-search-show');
        $("#close").addClass('glyphicon-remove-show');
        $("#close").removeClass('glyphicon-remove-hide');
        $(".form-control").focus();
    });
    $("#close").click(function(){
      $("#search-form").removeClass('form-group-change');
      $("#search").addClass('glyphicon-search-show');
      $("#search").removeClass('glyphicon-search-hide');
      $("#close").addClass('glyphicon-remove-hide');
      $("#close").removeClass('glyphicon-remove-show');
    });
    // fixed scroll menu
    var summaries = $('.scroll-to-fixed');
    summaries.each(function(i) {
      var summary = $(summaries[i]);
      var next = summaries[i + 1];
      summary.scrollToFixed({
        marginTop: $('#btn-show-menu').outerHeight(true) + 10,
        limit: function() {
          var limit = 0;
          if (next) {
              limit = $(next).offset().top - $(this).outerHeight(true) - 10;
          } else {
              limit = $('.main-footer').offset().top - $(this).outerHeight(true) - 10;
          }
          return limit;
        },
        zIndex: 999
      });
    });
    // fixed scroll category
  var summaries = $('.scroll-fixed-right');
    summaries.each(function(i) {
      var summary = $(summaries[i]);
      var next = summaries[i + 1];
      summary.scrollToFixed({
        marginTop: $('#btn-show-menu').outerHeight(true) + 10,
        limit: function() {
          var limit = 0;
          if (next) {
              limit = $(next).offset().top - $(this).outerHeight(true) - 10;
          } else {
              limit = $('.padding').offset().top - $(this).outerHeight(true) - 10;
          }
          return limit;
        },
        zIndex: 999
      });
    });

    // fixed scroll video
    var summaries = $('.scroll-fixed-video');
    summaries.each(function(i) {
      var summary = $(summaries[i]);
      var next = summaries[i + 1];
      summary.scrollToFixed({
        marginTop: $('#btn-show-menu').outerHeight(true) + 10,
        limit: function() {
          var limit = 0;
          if (next) {
            limit = $(next).offset().top - $(this).outerHeight(true) - 10;
          } else {
            limit = $('.main-footer').offset().top - $(this).outerHeight(true) - 10;
          }
          return limit;
        },
        zIndex: 999
      });
    });
    // fixed scroll highlight
    var summaries = $('.cate-fixtop');
    summaries.each(function(i) {
      var summary = $(summaries[i]);
      var next = summaries[i + 1];
      summary.scrollToFixed({
        marginTop: $('#btn-show-menu').outerHeight(true) + 10,
        limit: function() {
          var limit = 0;
          if (next) {
            limit = $(next).offset().top - $(this).outerHeight(true) - 10;
          } else {
            limit = $('.padding').offset().top - $(this).outerHeight(true) - 10;
          }
          return limit;
        },
        zIndex: 999
      });
    });
    // fixed scroll category
    var summaries = $('.scroll-fix-right');
    summaries.each(function(i) {
      var summary = $(summaries[i]);
      var next = summaries[i + 1];
      summary.scrollToFixed({
        marginTop: $('#btn-show-menu').outerHeight(true) + 10,
        limit: function() {
          var limit = 0;
          if (next) {
            limit = $(next).offset().top - $(this).outerHeight(true) - 10;
          } else {
            limit = $('.padding').offset().top - $(this).outerHeight(true) - 10;
          }
          return limit;
        },
        zIndex: 999
      });
    });
    /*kul game*/
     var summaries = $('.scroll-fixed-game');
      summaries.each(function(i) {
        var summary = $(summaries[i]);
        var next = summaries[i + 1];
        summary.scrollToFixed({
          marginTop: $('#btn-show-menu').outerHeight(true) + 10,
          limit: function() {
            var limit = 0;
            if (next) {
              limit = $(next).offset().top - $(this).outerHeight(true) - 10;
            } else {
              limit = $('.main-footer').offset().top - $(this).outerHeight(true) - 10;
            }
            return limit;
          },
          zIndex: 999
        });
      });
    /*kul game*/
    var summaries = $('.sticky-right');
      summaries.each(function(i) {
        var summary = $(summaries[i]);
        var next = summaries[i + 1];
        summary.scrollToFixed({
          marginTop: $('#btn-show-menu').outerHeight(true) + 10,
          limit: function() {
            var limit = 0;
            if (next) {
              limit = $(next).offset().top - $(this).outerHeight(true) - 10;
            } else {
              limit = $('.main-footer').offset().top - $(this).outerHeight(true) - 10;
            }
            return limit;
          },
          zIndex: 999
        });
      });
    var summaries = $('.sticky-left');
      summaries.each(function(i) {
        var summary = $(summaries[i]);
        var next = summaries[i + 1];
        summary.scrollToFixed({
          marginTop: $('#btn-show-menu').outerHeight(true) + 10,
          limit: function() {
            var limit = 0;
            if (next) {
              limit = $(next).offset().top - $(this).outerHeight(true) - 10;
            } else {
              limit = $('.main-footer').offset().top - $(this).outerHeight(true) - 10;
            }
            return limit;
          },
          zIndex: 999
        });
      });  
    // animation play
    /*var time = 10;
    var initialOffset = '220';
    var i = 1
    $(".stop-auto-play").click(function(){
        $('.circle_animation1').css('display',"block");
        $('.circle_animation').css('display',"none");
    });
    $('.circle_animation').css('stroke-dashoffset', initialOffset-(1*(initialOffset/time)));
      var interval = setInterval(function() {
        if (i == time){
          clearInterval(interval);
          return;
        }
        $('.circle_animation').css('stroke-dashoffset', initialOffset-((i+1)*(initialOffset/time)));
        i++;  
    },1000); */
});

