  /*kul beauty*/     
     $('.slider-for-beauty').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      asNavFor: '.slider-nav-beauty'
    });

    $('.slider-nav-beauty').slick({
      arrows: false,
      slidesToShow: 4,
      slidesToScroll: 1,
      asNavFor: '.slider-for-beauty',
      focusOnSelect: true,
      centerMode: true,
    });
   // slider video
    $('.medium-slider').slick({
        dots: false,
        autoplaySpeed: 2000,
        slidesToShow: 5,
        slidesToScroll: 5,
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4,
            dots: false
          }
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4,
            dots: false
          }
        },
      ]
    });
  /*kul tech*/
      $('.block-slide-tech').slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            dots: false
          }
        },
      ]
      });
    /*end kul tech*/